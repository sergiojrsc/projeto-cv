const I = actor();
// fragments/modal.js
module.exports = {

  div:{
    productName: '//div[@class="inventory_item_name "]',
    $addToCart: '//div[@class="inventory_item"][div[@class="inventory_item_description"]/div[@class="inventory_item_label"]/a/div[@class="inventory_item_name "][contains(text(), "$productName")]]//button[contains(text(), "Add to cart")]',
    numberProductAddToCart: '//div[@id="shopping_cart_container"]//span'
  },
  
  // we are clicking "Accept: inside a popup window
  getListNameAllProducts() {
    I.waitForElement(this.div.productName);
    return I.grabTextFromAll(this.div.productName);
  },
  clickAddToCartProduct(productName){
    I.waitForElement(this.div.$addToCart.replace('$productName',productName));
    console.log(this.div.$addToCart.replace('$productName',productName))
    I.click(this.div.$addToCart.replace('$productName',productName))
  },
  getQuantityProducts(){
    I.waitForElement(this.div.productName);
    const quantity = I.grabTextFromAll(this.div.productName);
    return quantity
  },
  getNumberProductAddToCart(){
    I.waitForElement(this.div.numberProductAddToCart);
    const quantityProductsAdd = I.grabTextFromAll(this.div.numberProductAddToCart);
    return quantityProductsAdd
  }
}
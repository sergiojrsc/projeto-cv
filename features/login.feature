# language:pt
Funcionalidade: Login
  Como usuário do sistema Swag Labs
  Desejo realizar o login no sistema
  Para realizar minhas tarefas

  Cenário: Login com sucesso
    Dado acesse a pagina "Swag Labs"
    E informe o usuário "standard_user"
    E informe a senha "secret_sauce"
    Quando clico no botão login
    Então o sistema apresenta a tela de "Products"

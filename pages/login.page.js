const I = actor();

module.exports = {

  div:{
    logo:'//div[@class="login_logo"]',
  }, 
  input:{
    userName: '//input[@id="user-name"]',
    password: '//input[@id="password"]',
    login:'//input[@id="login-button"]'
  },
  span:{
    title:'//span[@class="title"]'
  },
  
  accessPage(pageLogo) {
    I.amOnPage('/');
    I.waitForElement(this.div.logo);
    I.seeTextEquals(pageLogo, this.div.logo);
  },
  setUser(user){
    I.waitForElement(this.input.userName);
    I.fillField(this.input.userName,user);
  },
  setPassword(password){
    I.waitForElement(this.input.password);
    I.fillField(this.input.password,password);
  },
  clickLogin(){
    I.waitForElement(this.input.login);
    I.click(this.input.login);
  },
  assertPageTitle(pageTitle){
    I.waitForElement(this.span.title);
    I.seeTextEquals(pageTitle,this.span.title);
  }
}

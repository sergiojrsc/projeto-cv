const expect = require('expect.js');

const loginPage = require('../pages/login.page');

Given ('acesse a pagina {string}', async(page)=>{
  await loginPage.accessPage(page);
});

Given ('informe o usuário {string}', async(user)=>{
  await loginPage.setUser(user);
});

Given ('informe a senha {string}', async(password)=>{
  await loginPage.setPassword(password);
});

Given ('acessar a pagina {string} com o usuário {string}', async(page,user)=>{
  await loginPage.accessPage(page);
  await loginPage.setUser(user);
  await loginPage.setPassword('secret_sauce');
  await loginPage.clickLogin();
  await loginPage.assertPageTitle("Products");
});

When ('clico no botão login', async()=>{
  await loginPage.clickLogin();
});

Then ('o sistema apresenta a tela de {string}', async(page)=>{
  await loginPage.assertPageTitle(page);
});
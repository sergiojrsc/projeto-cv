const expect = require('expect.js');
const _ =require('lodash');

const productsPage = require('../pages/produtcs.page');

Given ('adicionar todos os produtos no carrinho', async()=>{
    let listNameProducts = []
    listNameProducts = await productsPage.getListNameAllProducts();
    for (const key in listNameProducts) {
        await productsPage.clickAddToCartProduct(listNameProducts[key])
    }
});

Then ('a quantidade de produtos adicionado no carrinho deve ser a mesma quantidade de produtos exibido', async()=>{
    const quantityProduct = await productsPage.getQuantityProducts();
    const quantityAddToCart = _.toNumber(await productsPage.getNumberProductAddToCart());
    expect(quantityAddToCart).to.equal(quantityProduct.length);
});

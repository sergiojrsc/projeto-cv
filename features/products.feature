# language:pt
Funcionalidade: Products
  Como usuário do sistema Swag Labs
  Desejo acessar a pagina de produtos
  Para adicionar os produtos no carrinho

  Esquema do Cenário: Adicionar todos os produtos no carrinho
    Dado acessar a pagina "Swag Labs" com o usuário <user>
    Quando adicionar todos os produtos no carrinho
    Então a quantidade de produtos adicionado no carrinho deve ser a mesma quantidade de produtos exibido
    Exemplos:
    | user            | 
    | 'standard_user' |
    | 'problem_user'  |
    